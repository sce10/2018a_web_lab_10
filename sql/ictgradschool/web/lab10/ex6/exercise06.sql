DROP TABLE IF EXISTS dbtest_tableex6;

CREATE TABLE IF NOT EXISTS dbtest_tableex6 (
    id INT NOT NULL AUTO_INCREMENT,
    title VARCHAR(32),
    director VARCHAR(32),
    wrate FLOAT,
    rentedby INT,
    PRIMARY KEY (id),
    FOREIGN KEY (rentedby) REFERENCES dbtest_tableex3 (id)
);

INSERT INTO dbtest_tableex6 (title, director, wrate) VALUES
    ('Jaws', 'Steven Spielberg', 2),
    ('Schindlers List', 'Steven Spielberg', 4),
    ('The Shape of Water', 'Guillermo Del Toro', 4),
    ('This is 40', 'Judd Appatow', 6);


-- INSERT INTO dbtest_tableex6 (title, director, wrate, rentedby) VALUES
--     ('Jawsdddd', 'Steven Spielberg', 2, 4),
--     ('Schindlers Listdddd', 'Stedven Spielberg', 4, 4),
--     ('The Shape of Waterdddd', 'Guillermo Del Toro', 4, 2),
--     ('This is 40dddddd', 'Judd Appatow', 6, 7);

SELECT * FROM dbtest_tableex6;-- Answers to Exercise 6 here


SELECT COUNT(*) -- Number of rows
  FROM dbtest_tableex6 AS movie, dbtest_tableex3 AS person -- Aliasing tables
  WHERE movie.rentedby = person.id -- Joining the two tables together by FK
    AND person.name LIKE 'Tem%';
