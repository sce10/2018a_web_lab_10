-- Answers to Exercise 2 here
DROP TABLE IF EXISTS dbtest_tableseamus;

CREATE TABLE IF NOT EXISTS dbtest_tableseamus (
    username VARCHAR(15) NOT NULL,
    first_name VARCHAR(32),
    last_name VARCHAR(32),
    email VARCHAR(50),
    PRIMARY KEY (username)
);

INSERT INTO dbtest_tableseamus (username, first_name, last_name, email) VALUES
    ('programmer1', 'Bill', 'Gates', 'bill@microsoft.com'),
    ('programmer2', 'Peter', 'Gates', 'bill@microsoft.com'),
    ('programmer3', 'Pete', 'Gates', 'bill@microsoft.com'),
    ('programmer4', 'Peterson', 'Gates', 'bill@microsoft.com');

SELECT * FROM dbtest_tableseamus;
