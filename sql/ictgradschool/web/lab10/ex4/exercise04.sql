-- Answers to Exercise 4 here

DROP TABLE IF EXISTS dbtest_tableex4;

CREATE TABLE IF NOT EXISTS dbtest_tableex4 (
    id INT NOT NULL AUTO_INCREMENT,
    arttitle VARCHAR(64),
    arttext VARCHAR(10000),
    PRIMARY KEY (id)
);

-- Here again, we insert 6 rows of data, but this time we use a single INSERT
-- statement. Run this INSERT after creating the table above, and compare the
-- time taken to that recorded from the individual inserts.
INSERT INTO dbtest_tableex4 (arttitle, arttext) VALUES
    ('What is Lorem Ipsum?', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'),
    ('Why do we use it?', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).');

SELECT * FROM dbtest_tableex4;
