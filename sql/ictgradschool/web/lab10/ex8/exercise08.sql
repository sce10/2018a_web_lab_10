-- Answers to Exercise 8 here
DELETE FROM dbtest_tableex7
WHERE  dbtest_tableex7.id = 1;

ALTER TABLE dbtest_tableex7 DROP comment;

DROP TABLE dbtest_tableex5;

UPDATE dbtest_tableex3
SET name = "peterpeter"
WHERE year_born = 1961;

UPDATE dbtest_tableex3
SET year_born = 1962
WHERE name = "peterpeter";

UPDATE dbtest_tableex5
SET username = "Seamus"
WHERE first_name = "Bill";
