-- Answers to Exercise 9 here
SELECT * FROM dbtest_tableex3;

SELECT dbtest_tableex3.id, dbtest_tableex3.gender, dbtest_tableex3.year_born, dbtest_tableex3.joined FROM dbtest_tableex3;

SELECT dbtest_tableex4.arttitle FROM dbtest_tableex4;

SELECT DISTINCT dbtest_tableex6.director FROM dbtest_tableex6;

SELECT dbtest_tableex6.title FROM dbtest_tableex6 WHERE dbtest_tableex6.wrate <= 2;

SELECT dbtest_tableex5.username FROM dbtest_tableex5 ORDER BY dbtest_tableex5.username;

SELECT dbtest_tableex5.first_name FROM dbtest_tableex5 WHERE dbtest_tableex5.first_name LIKE "pete%";

SELECT dbtest_tableex5.first_name FROM dbtest_tableex5 WHERE dbtest_tableex5.first_name LIKE "pete%" OR dbtest_tableex5.last_name LIKE "pete%";
